<section class="hero layout" data-module="hero">
  <main class="hero__container container">
    <div class="hero__intro">
      <div class="hero__content">
        <h1 class="hero__headline"><?php echo nl2br( get_field( 'index__headline' ) ); ?></h1>
        <?php the_content(); ?>
      </div>

      <?php
      the_module( 'image', [
        'id'    => get_field( 'index__product' ),
        'class' => 'hero__product',
      ] );
      ?>
    </div>

    <?php
    $recent = new WP_Query( 'posts_per_page=1' );

    while ( $recent->have_posts() ) {

      $recent->the_post();

      the_module( 'post', [
        'featured' => true,
        'excerpt' => true,
      ] );
    }
    wp_reset_postdata(); ?>

    <?php
    the_module( 'image', [
      'id'    => get_post_thumbnail_id( $post->ID ),
      'class' => 'hero__background',
    ] );
    ?>
  </main>
</section>
