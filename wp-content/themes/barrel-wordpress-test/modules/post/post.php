<?php
/**
 * Post block
 *
 * @author BarrelNY
 */

$post__class = 'post--loop';
$post__image_class = '';
$post__meta_class = '';
if ( $featured === true ) {
  $post__class = 'post--latest';
  $post__image_class = 'post__image--latest';
  $post__meta_class = 'post__meta--latest';
}
$post__image = featured_image_or_fallback( $post );
$post__image_position = image_custom_position( $post );
$post__icon = get_format_icon( $post );
$post__cta = get_cta( $post ); ?>

  <article class="post <?php echo esc_attr( $post__class ); ?>">
    <a href="<?php the_permalink(); ?>" class="post__image post--link <?php echo esc_attr( $post__image_class ); ?>">
    <span
      style="background-image:url('<?php echo $post__image; ?>');background-position:<?php echo $post__image_position; ?>">
    </span>
    </a>
    <div class="post__meta <?php echo esc_attr( $post__meta_class ); ?>">
      <div class="post__meta--icon">
        <?php echo $post__icon; ?>
      </div>
      <a href="<?php echo get_month_link( get_the_date( 'Y' ), get_the_date( 'm' ) ); ?>" class="post__meta--date">
        <button><?php echo get_the_date( 'F d' ); ?></button>
      </a>
      <a href="<?php the_permalink(); ?>" class="post__meta--title post--link">
        <h4><?php the_title(); ?></h4>
      </a>
      <?php if($excerpt === true) : ?>
        <div class="post__meta--excerpt">
          <?php the_excerpt(); ?>
        </div>
      <?php endif; ?>
      <a href="<?php the_permalink(); ?>" class="post__meta--cta post--link">
        <?php echo $post__cta; ?>
      </a>
    </div>
  </article>
<?php
