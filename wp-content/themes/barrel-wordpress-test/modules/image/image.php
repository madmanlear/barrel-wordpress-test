<?php
$module = 'data-module="image"';
if ( ! empty( $cover ) ) {
  $class .= ' image--cover';
}
if ( ! empty( $contain ) ) {
  $class .= ' image--contain';
}
if ( ! empty( $top ) ) {
  $class .= ' image--top';
}
if ( empty( $sizes ) ) {
  $sizes = '';
}
if ( empty( $attributes ) ) {
  $attributes = '';
}
if ( empty( $alt ) ) {
  $alt = '';
}
if ( ! isset( $use_srcset ) ) {
  $use_srcset = true;
}
?>
<figure class="image <?= $class ?>" <?= $module; ?> <?= $attributes; ?>>
  <?php
  if ( ! empty( $id ) ) {
    echo wp_get_attachment_image( $id, $size, false, [
      'loading' => 'lazy',
      'alt'     => $alt,
    ] );
  }

  if ( ! empty( $content ) ) {
    echo $content;
  }
  ?>
</figure>
